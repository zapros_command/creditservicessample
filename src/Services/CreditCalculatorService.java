package Services;

import Domain.Bank;
import Domain.BankToCreditTypeRel;
import Domain.CreditRequestModel;
import Domain.CreditType;

import java.util.ArrayList;

/**
 * Created by promazol on 15.11.2016.
 */
public class CreditCalculatorService implements ICreditCalculatorService{


    public ArrayList<Double> Calculate(CreditRequestModel model) {

        double percent = this.GetPercent(model.Bank, model.CreditType);
        if (percent == 0) {
            throw new IllegalArgumentException("Невозможно найти процент для данной операции");
        }

        return this.CalculateByParams(model.PaymentType.Id, model.Months, percent, model.Amount);
    }

    public ArrayList<Double> CalculateByParams(int paymentType, double months, double percent, double amount){
        ArrayList<Double> monthAmounts = new ArrayList<Double>();
        if (paymentType == 1) {
            double sum = (
                    (
                            Math.pow((1 + percent / 12), months) * (percent / 12)
                    )
                            /
                            (
                                    Math.pow((1 + percent / 12), months) - 1)
            )
                    * amount;

            monthAmounts.add(sum);
        } else {

            for (int j = 1; j < months + 1; j++) {
                double sum = (
                        amount / months
                                +
                                (amount - (amount / months) * (j - 1))
                                        *
                                        (percent / 12));

                monthAmounts.add(sum);
            }
        }

        return monthAmounts;
    }

    public double GetPercent(Bank bank, CreditType creditType){
        BankToCreditTypeRel[] bankToCreditTypeRels = bank.BankToCreditTypeRels;
        double percent = 0;

        for (int i = 0; i <= bankToCreditTypeRels.length; i++) {
            BankToCreditTypeRel bankToCreditTypeRel = bankToCreditTypeRels[i];
            if(bankToCreditTypeRel.CreditType.Id == creditType.Id){
                percent = bankToCreditTypeRel.Percent;
                break;
            }
        }

        return percent;
    }

}
