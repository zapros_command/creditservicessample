package Domain;

/**
 * Created by promazol on 15.11.2016.
 */
public class BankToCreditTypeRel {
    public double Percent;
    public CreditType CreditType;

    public BankToCreditTypeRel(Domain.CreditType creditType, double percent) {
        Percent = percent;
        CreditType = creditType;
    }
}
