package Domain;

import java.util.ArrayList;

/**
 * Created by promazol on 15.11.2016.
 */
public class Bank extends EntityBase {
    public BankToCreditTypeRel[] BankToCreditTypeRels;

    public Bank(int id, String name, BankToCreditTypeRel[] bankToCreditTypeRels) {
        super(id, name);
        this.BankToCreditTypeRels = bankToCreditTypeRels;
    }
}
