package Domain;

/**
 * Created by promazol on 15.11.2016.
 */
public class CreditRequestModel {
    public Double Amount;
    public Double Months;
    public Bank Bank;
    public CreditType CreditType;
    public PaymentType PaymentType;
}
