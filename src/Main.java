package qqqqq;

import Dal.BanksSet;
import Dal.CreditTypeSet;
import Dal.PaymentTypeSet;
import Domain.CreditRequestModel;
import Services.CreditCalculatorService;
import Services.ICreditCalculatorService;

import java.util.ArrayList;
import java.util.Scanner;

/*
 * Воспользовался методом Сканнер
 *
 * Беру условно, что прохожу по всем стоп-факторам кредитных историй и отличная репутация в базах данных
 * в Бюро кредитных историй.
 *
 *  Беру,что выдадут любую сумму, будет меняться только % платежа по типу кредита и по названию банка
 *
 *  Не прорабатываю ошибки ввода не числовых значений, при вводе текста процесс будет вылетать
*/

public class Main {
    static Scanner sc = new Scanner(System.in);

    public static void main(String args[]) {
        CreditRequestModel requestModel = new CreditRequestModel();

        System.out.print("Введите сумму кредита  :");
        requestModel.Amount = sc.nextDouble();

        System.out.print("Срок выплаты кредита (в месяцах)  :");
        requestModel.Months = sc.nextDouble();

        BanksSet banksSet = BanksSet.getInstance();
        System.out.print(banksSet.GetHelp());
        requestModel.Bank = banksSet.GetById(sc.nextInt());

        CreditTypeSet creditTypeSet = CreditTypeSet.getInstance();
        System.out.print(creditTypeSet.GetHelp());
        requestModel.CreditType = creditTypeSet.GetById(sc.nextInt());


        PaymentTypeSet paymentTypeSet = PaymentTypeSet.getInstance();
        System.out.print(paymentTypeSet.GetHelp());
        requestModel.PaymentType = paymentTypeSet.GetById(sc.nextInt());

        ICreditCalculatorService calculatorService = new CreditCalculatorService();
        ArrayList<Double> calculation = calculatorService.Calculate(requestModel);

        if (requestModel.PaymentType.Id == 1) {
            System.out.println("Сумма ежемесячного платежа: " + calculation.get(0));
        } else {
            for (int i = 0; i < calculation.size(); i++) {
                System.out.println("Сумма платежа за " + (i + 1) + " месяц " + calculation.get(i));
            }
        }
    }
}

