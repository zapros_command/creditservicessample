package Dal;

import Domain.EntityBase;

import java.util.ArrayList;

/**
 * Created by promazol on 15.11.2016.
 */
public abstract class DataSet<T extends EntityBase> {
    protected ArrayList<T> Data;

    public DataSet() {
        this.Data = new ArrayList<T>();
        this.FillData();
    }

    protected abstract void FillData();

    public ArrayList<T> GetData() {
        return this.Data;
    }

    public T GetById(int id) {
        T data = null;
        for (int i = 0; i < this.Data.size(); i++) {
            T t = this.Data.get(i);
            if (t.Id == id) {
                data = t;
                break;
            }
        }

        if (data == null) {
            throw new IllegalArgumentException();
        }

        return data;
    }
}
