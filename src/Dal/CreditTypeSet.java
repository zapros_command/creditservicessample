package Dal;

import Domain.CreditType;

import java.util.Iterator;

/**
 * Created by promazol on 15.11.2016.
 */
public class CreditTypeSet extends DataSet<CreditType> {
    private static CreditTypeSet ourInstance = new CreditTypeSet();
    public static CreditTypeSet getInstance() {
        return ourInstance;
    }

    @Override
    protected void FillData() {
        this.Data.add(new CreditType(1, "Потребительский"));
        this.Data.add(new CreditType(2, "Автокредит"));
        this.Data.add(new CreditType(3, "Ипотека:вторичка"));
        this.Data.add(new CreditType(4, "Ипотека:новое жилье"));
        this.Data.add(new CreditType(5, "Ипотека под залог недвиги"));
    }

    public String GetHelp(){
        String bankHelp = "Введите номер вида кредита: ";

        for (Iterator<CreditType> i = this.Data.iterator(); i.hasNext();) {
            CreditType bank = i.next();
            bankHelp += bank.Id + " - " + bank.Name;
            if (i.hasNext()) {
                bankHelp += ", ";
            } else {
                bankHelp += ": ";
            }
        }

        return bankHelp;
    }
}